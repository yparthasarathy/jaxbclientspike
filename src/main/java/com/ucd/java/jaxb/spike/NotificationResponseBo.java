
package com.ucd.java.jaxb.spike;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for notificationResponseBo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="notificationResponseBo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://spike.jaxb.java.ucd.com/}notification">
 *       &lt;sequence>
 *         &lt;element name="bo" type="{http://spike.jaxb.java.ucd.com/}notificationBo" minOccurs="0"/>
 *         &lt;element name="status" type="{http://spike.jaxb.java.ucd.com/}notificationStatusEnum" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "notificationResponseBo", propOrder = {
    "bo",
    "status"
})
public class NotificationResponseBo
    extends Notification
{

    protected NotificationBo bo;
    protected NotificationStatusEnum status;

    /**
     * Gets the value of the bo property.
     * 
     * @return
     *     possible object is
     *     {@link NotificationBo }
     *     
     */
    public NotificationBo getBo() {
        return bo;
    }

    /**
     * Sets the value of the bo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificationBo }
     *     
     */
    public void setBo(NotificationBo value) {
        this.bo = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link NotificationStatusEnum }
     *     
     */
    public NotificationStatusEnum getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificationStatusEnum }
     *     
     */
    public void setStatus(NotificationStatusEnum value) {
        this.status = value;
    }

}
