
package com.ucd.java.jaxb.spike;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for notificationStatusEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="notificationStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SENT"/>
 *     &lt;enumeration value="PENDING"/>
 *     &lt;enumeration value="FATAL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "notificationStatusEnum")
@XmlEnum
public enum NotificationStatusEnum {

    SENT,
    PENDING,
    FATAL;

    public String value() {
        return name();
    }

    public static NotificationStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
