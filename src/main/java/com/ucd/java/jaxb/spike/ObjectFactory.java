
package com.ucd.java.jaxb.spike;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ucd.java.jaxb.spike package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NotificationBo_QNAME = new QName("http://spike.jaxb.java.ucd.com/", "notificationBo");
    private final static QName _NotificationResponseBo_QNAME = new QName("http://spike.jaxb.java.ucd.com/", "notificationResponseBo");
    private final static QName _Notification_QNAME = new QName("http://spike.jaxb.java.ucd.com/", "notification");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ucd.java.jaxb.spike
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NotificationBo }
     * 
     */
    public NotificationBo createNotificationBo() {
        return new NotificationBo();
    }

    /**
     * Create an instance of {@link Notification }
     * 
     */
    public Notification createNotification() {
        return new Notification();
    }

    /**
     * Create an instance of {@link NotificationResponseBo }
     * 
     */
    public NotificationResponseBo createNotificationResponseBo() {
        return new NotificationResponseBo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotificationBo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://spike.jaxb.java.ucd.com/", name = "notificationBo")
    public JAXBElement<NotificationBo> createNotificationBo(NotificationBo value) {
        return new JAXBElement<NotificationBo>(_NotificationBo_QNAME, NotificationBo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotificationResponseBo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://spike.jaxb.java.ucd.com/", name = "notificationResponseBo")
    public JAXBElement<NotificationResponseBo> createNotificationResponseBo(NotificationResponseBo value) {
        return new JAXBElement<NotificationResponseBo>(_NotificationResponseBo_QNAME, NotificationResponseBo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Notification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://spike.jaxb.java.ucd.com/", name = "notification")
    public JAXBElement<Notification> createNotification(Notification value) {
        return new JAXBElement<Notification>(_Notification_QNAME, Notification.class, null, value);
    }

}
