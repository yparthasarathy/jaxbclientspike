
package com.ucd.java.jaxb.spike;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for templateEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="templateEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BUNDLE_FORWARD_INVITE_SEND"/>
 *     &lt;enumeration value="BUNDLE_FORWARD_INVITE_RESOLVED"/>
 *     &lt;enumeration value="BUNDLE_REVERSE_INVITE_SEND"/>
 *     &lt;enumeration value="BUNDLE_REVERSE_INVITE_RESOLVED"/>
 *     &lt;enumeration value="BATCH_JOB_EXIT_STATUS"/>
 *     &lt;enumeration value="FREE_TEXT_NOTIFICATION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "templateEnum")
@XmlEnum
public enum TemplateEnum {

    BUNDLE_FORWARD_INVITE_SEND,
    BUNDLE_FORWARD_INVITE_RESOLVED,
    BUNDLE_REVERSE_INVITE_SEND,
    BUNDLE_REVERSE_INVITE_RESOLVED,
    BATCH_JOB_EXIT_STATUS,
    FREE_TEXT_NOTIFICATION;

    public String value() {
        return name();
    }

    public static TemplateEnum fromValue(String v) {
        return valueOf(v);
    }

}
