package com.ucd.java.jaxb.spike.main;

import com.ucd.java.jaxb.spike.NotificationBo;
import com.ucd.java.jaxb.spike.SushiNotificationService;
import com.ucd.java.jaxb.spike.SushiNotificationServiceService;
import com.ucd.java.jaxb.spike.TemplateEnum;


/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		SushiNotificationServiceService service = new SushiNotificationServiceService();
		SushiNotificationService notify = service
				.getSushiNotificationServicePort();
		NotificationBo request = new NotificationBo();
		request.setCc("cc@test.com");
		request.setId("some random id");
		request.setRecipientsEmails("recipient@test.com");
		request.setReplyTo("replyto@test.com");
		request.setTemplate(TemplateEnum.BATCH_JOB_EXIT_STATUS);
		request.setTemplateVars("{vars : sample json vars}");

		String value = notify.createNotification(request);
		System.out.println("value: " + value);

		/*
		 * NotificationResponseBo value = (NotificationResponseBo)
		 * notify.getNotification("asdas"); System.out.println("value: %o" +
		 * value.getStatus()); System.out.println("value: %o" +
		 * value.getBo().getCc()); System.out.println("value: %o" +
		 * value.getBo().getTemplateVars());
		 */

	}
}
